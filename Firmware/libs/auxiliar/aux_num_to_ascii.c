/************************************************************************/
/* Conversions                                                          */
/************************************************************************/
void u8_2_ascii(char *ascii, uint8_t u8);			// 3 chars
void u16_2_ascii(char *ascii, uint16_t u16);	// 5 chars
void i16_2_ascii(char *ascii, int16_t i16);		// 6 chars
void i12_2_ascii(char *ascii, int16_t i12);		// 5 chars

void u8_2_ascii(char *ascii, uint8_t u8)
{
	ascii[0] = u8/100 + 0x30;
	u8       = u8%100;
	ascii[1] = u8/10 + 0x30;
	ascii[2] = u8%10 + 0x30;
}

void u16_2_ascii(char *ascii, uint16_t u16)
{
	ascii[0] = u16/10000 + 0x30;
	u16      = u16%10000;
	ascii[1] = u16/1000 + 0x30;
	u16      = u16%1000;
	ascii[2] = u16/100 + 0x30;
	u16      = u16%100;
	ascii[3] = u16/10 + 0x30;
	ascii[4] = u16%10 + 0x30;
}

void i16_2_ascii(char *ascii, int16_t i16)
{
	uint16_t u16;
	if (i16 < 0)
	{
		u16 = ~((uint16_t)i16) + 1;
		ascii[0] = '-';
	}
	else
	{
		u16 = (uint16_t)i16;
		ascii[0] = '+';
	}

	ascii[1] = u16/10000 + 0x30;
	u16      = u16%10000;
	ascii[2] = u16/1000 + 0x30;
	u16      = u16%1000;
	ascii[3] = u16/100 + 0x30;
	u16      = u16%100;
	ascii[4] = u16/10 + 0x30;
	ascii[5] = u16%10 + 0x30;
}

void i12_2_ascii(char *ascii, int16_t i12)
{
	uint16_t u12;
	if (i12 & 0x0800)
	{
		u12 = ((uint16_t)i12) | 0xF000;
		u12 = ~u12 + 1;
		ascii[0] = '-';
	}
	else
	{
		u12 = (uint16_t)i12 & 0x0FFF;
		ascii[0] = '+';
	}

	ascii[1] = u12/1000 + 0x30;
	u12      = u12%1000;
	ascii[2] = u12/100 + 0x30;
	u12      = u12%100;
	ascii[3] = u12/10 + 0x30;
	ascii[4] = u12%10 + 0x30;
}