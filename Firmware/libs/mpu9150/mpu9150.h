#ifndef _MPU9150_H_
#define _MPU9150_H_

#include <stdbool.h>
#include <stdint.h>
#include "..\nRF24LE1\nRF_i2c.h"

/************************************************************************/
/* Pins definition                                                      */
/************************************************************************/
#define INTpin 2
#define INTport_pin P02

/************************************************************************/
/* Register add                                                         */
/************************************************************************/
// MPU9150
#define SMPLRT_DIV		0x19
#define GYRO_CONFIG		0x1B
#define ACCEL_CONFIG	0x1C
#define INT_PIN_CFG		0x37
#define INT_ENABLE		0x38
#define INT_STATUS		0x3A
#define ACCEL_XOUT_H	0x3B
#define TEMP_OUT_H		0x41
#define GYRO_XOUT_H		0x43
#define PWR_MGMT_1		0x6B
#define PWR_MGMT_2		0x6C
#define WHO_AM_I			0x75

// AK8975
#define WIA						0x00
#define HXL						0x03
#define ST2						0x09
#define CNTL					0x0A

/************************************************************************/
/* Register contents                                                    */
/************************************************************************/
// MPU9150
#define SMPLRT_DIV_val_8Kz_50Hz  	159
#define SMPLRT_DIV_val_8Kz_100Hz 	79
#define SMPLRT_DIV_val_8Kz_200Hz 	39
#define SMPLRT_DIV_val_1Kz_50Hz  	19
#define SMPLRT_DIV_val_1Kz_100Hz 	9
#define SMPLRT_DIV_val_1Kz_200Hz 	4
#define GYRO_CONFIG_msk_250dps		0x00	// +/-250 degrees per second
#define GYRO_CONFIG_msk_500dps		0x08	// +/-500 degrees per second
#define GYRO_CONFIG_msk_1000dps		0x10	// +/-1000 degrees per second
#define GYRO_CONFIG_msk_2000dps		0x18	// +/-2000 degrees per second
#define ACCEL_CONFIG_msk_2g				0x00	// +/-2g
#define ACCEL_CONFIG_msk_4g				0x08	// +/-4g
#define ACCEL_CONFIG_msk_8g				0x10	// +/-8g
#define ACCEL_CONFIG_msk_16g			0x18	// +/-16g
#define INT_ENABLE_msk_data_rdy		0x01	// All data is ready
#define INT_ENABLE_msk_i2c_mst		0x08	// I2C master interrupt
#define INT_ENABLE_msk_fifo_oflow	0x10	// FIFO achive overflow
#define INT_STATUS_msk_data_rdy		0x01	// All data is ready
#define INT_STATUS_msk_i2c_mst		0x08	// I2C master interrupt
#define INT_STATUS_msk_fifo_oflow	0x10	// FIFO achive overflow
#define INT_PIN_CFG_msk_i2c_bypass_en 0x02// Pass-through mode if equal 1
#define INT_PIN_CFG_msk_int_rd_clear 0x10	// Interrupt status bits cleared on any read operation
#define INT_PIN_CFG_msk_latch_int_en 0x20	// The INT pin emits a 50us long pulse
#define INT_PIN_CFG_msk_int_level 0x80	// Logic level for the INT pin is active low
#define PWR_MGMT_1_msk_inter_8MHz	0x00	// Internall 8MHz oscillator
#define PWR_MGMT_1_msk_PLL_x_gyro	0x01	// PLL with X axis gyro reference
#define PWR_MGMT_1_msk_PLL_y_gyro	0x02	// PLL with Y axis gyro reference
#define PWR_MGMT_1_msk_PLL_z_gyro	0x03	// PLL with Z axis gyro reference
#define PWR_MGMT_1_msk_reset			0x80	// Reset device
#define PWR_MGMT_1_msk_sleep			0x40	// Sleep mode
#define PWR_MGMT_2_msk_stby_x_acc	0x20	// Standby accelerometer axis X
#define PWR_MGMT_2_msk_stby_y_acc	0x10	// Standby accelerometer axis Y
#define PWR_MGMT_2_msk_stby_z_acc	0x08	// Standby accelerometer axis Z
#define PWR_MGMT_2_msk_stby_x_gyr	0x04	// Standby gyroscope axis X
#define PWR_MGMT_2_msk_stby_y_gyr	0x02	// Standby gyroscope axis Y
#define PWR_MGMT_2_msk_stby_z_gyr	0x01	// Standby gyroscope axis Z
#define WHO_AM_I_val							0x71	// Upper 6 bits of I2C address

// AK8975
#define CNTL_msk_pwr_dwn					0x00	// Power down mode
#define CNTL_msk_single_meas			0x01	// Single measurement mode
#define CNTL_msk_self_test				0x08	// Self-test mode
#define CNTL_msk_rom_access				0x0F	// Fuse ROM access mode

/************************************************************************/
/* User                                                                 */
/************************************************************************/
#define mpu9150_int_read INTport_pin

/************************************************************************/
/* Defines                                                              */
/************************************************************************/
typedef enum {
	MPU1950_FREQ_50Hz = SMPLRT_DIV_val_8Kz_50Hz,
	MPU1950_FREQ_100Hz = SMPLRT_DIV_val_8Kz_100Hz,
	MPU1950_FREQ_200Hz = SMPLRT_DIV_val_8Kz_200Hz,
} mpu9150_freq_t;

typedef enum {
	MPU1950_RANGE_ACC_2g = ACCEL_CONFIG_msk_2g,
	MPU1950_RANGE_ACC_4g = ACCEL_CONFIG_msk_4g,
	MPU1950_RANGE_ACC_8g = ACCEL_CONFIG_msk_8g,
	MPU1950_RANGE_ACC_16g = ACCEL_CONFIG_msk_16g,
} mpu9150_range_acc_t;

typedef enum {
	MPU1950_RANGE_GYR_250dps = GYRO_CONFIG_msk_250dps,
	MPU1950_RANGE_GYR_500dps = GYRO_CONFIG_msk_500dps,
	MPU1950_RANGE_GYR_1000dps = GYRO_CONFIG_msk_1000dps,
	MPU1950_RANGE_GYR_2000dps = GYRO_CONFIG_msk_2000dps,
} mpu9150_range_gyr_t;

typedef enum {
	MPU1950_RANGE_MAG_undifined,
} mpu9150_range_mag_t;


/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
void mpu9150_init(void);

bool mpu9150_exist(i2c_dev_t dev);
void mpu9150_reset(i2c_dev_t dev);
void mpu9150_sleep(i2c_dev_t dev);
bool mpu9150_start(
	i2c_dev_t dev,
	uint8_t freq,
	uint8_t use_acc,
	uint8_t use_gyr,
	uint8_t use_mag,
	uint8_t range_acc,
	uint8_t range_gyr,
	uint8_t range_mag);

void mpu9150_mag_trig_single_meas(i2c_dev_t dev);
void mpu9150_mag_read_meas(i2c_dev_t dev, uint8_t * axis);
	
#endif /* _MPU9150_H_ */