#include "mpu9150.h"

#include <stdint.h>
#include <stdbool.h>
#include <reg24le1.h>
#include "..\nRF24LE1\nRF_i2c.h"

void mpu9150_init(void)
{
	/* INT */
	P0DIR |= (1<<INTpin);		// To in
	P0CON = 0x10 | INTpin;	// In: No pull up/down resistors
}

bool mpu9150_exist(i2c_dev_t dev)
{
	dev.reg = WHO_AM_I;
	nRF_i2c_rReg(&dev, 1);
	return (dev.reg_val == WHO_AM_I_val ) ? true : false;
}

void mpu9150_reset(i2c_dev_t dev)
{
	dev.reg = PWR_MGMT_1;
	dev.reg_val = PWR_MGMT_1_msk_reset;
	nRF_i2c_wReg(&dev);
}

void mpu9150_sleep(i2c_dev_t dev)
{
	dev.reg = PWR_MGMT_1;
	dev.reg_val = PWR_MGMT_1_msk_sleep;
	nRF_i2c_wReg(&dev);
}

bool mpu9150_start(
	i2c_dev_t dev,
	uint8_t freq,
	uint8_t use_acc,
	uint8_t use_gyr,
	uint8_t use_mag,
	uint8_t range_acc,
	uint8_t range_gyr,
	uint8_t range_mag)
{
	/* Save MPU9150 address*/
	uint8_t mpu9150_add = dev.add;
	
	/* External voltages read at this point */
	// CPOUT: 0.6 V
	// REGOUT: 0 V
	
	/* Configure Power Mode and Clock Source */
	// SLEEP = 0			No sleep mode
	// CYCEL = 0			Cycle between sleep and acquire sample is not used
	// TEMP_DIS = 0 	Temperature sensor is enabled
	// CLKSEL = 1			PLL with X axis gyro reference
	dev.reg = PWR_MGMT_1;
	dev.reg_val = PWR_MGMT_1_msk_PLL_x_gyro;
	nRF_i2c_wReg(&dev);
	nRF_i2c_rReg(&dev, 1);
	if (dev.reg_val != PWR_MGMT_1_msk_PLL_x_gyro)
	{
		mpu9150_reset(dev);
		return false;
	}
	

	if (use_mag)
	{
		/* Enable I2C Pass through mode */
		dev.reg = INT_PIN_CFG;
		dev.reg_val = INT_PIN_CFG_msk_i2c_bypass_en;
		nRF_i2c_wReg(&dev);	
		
		/* AK8975 address*/
		// 0x0C << 1 = 0x18
		dev.add = 0x18;
		
		/* Read AK8975 Who Am I (WIA) */
		dev.reg = WIA;
		nRF_i2c_rReg(&dev, 1);	
		if (dev.reg_val != 0x48)
		{
			return false;
		}
		
		/* Disable I2C Pass through mode */
		dev.reg = INT_PIN_CFG;
		dev.reg_val = 0;
		nRF_i2c_wReg(&dev);
	}
	
	/* Recover MPU9150 address*/
	dev.add = mpu9150_add;
	
	

	/* Disable gyroscope if not used*/
	// STBY_XG = 1		Disable gyro X axis
	// STBY_YG = 1		Disable gyro Y axis
	// STBY_YG = 1		Disable gyro Z axis
	if (!use_acc)
	{
		dev.reg = PWR_MGMT_2;
		dev.reg_val = PWR_MGMT_2_msk_stby_x_acc | PWR_MGMT_2_msk_stby_y_acc | PWR_MGMT_2_msk_stby_z_acc;
		nRF_i2c_wReg(&dev);
		nRF_i2c_rReg(&dev, 1);
	}
	
	/* Disable accelerometer if not used*/
	// STBY_XA = 1		Disable acc X axis
	// STBY_YA = 1		Disable acc Y axis
	// STBY_YA = 1		Disable acc Z axis
	if (!use_gyr)
	{
		dev.reg = PWR_MGMT_2;
		dev.reg_val = PWR_MGMT_2_msk_stby_x_gyr | PWR_MGMT_2_msk_stby_y_gyr | PWR_MGMT_2_msk_stby_z_gyr;
		nRF_i2c_wReg(&dev);
		nRF_i2c_rReg(&dev, 1);
	}
	

	/* External voltages read at this point */
	// CPOUT: 25 V
	// REGOUT: 1.8 V
	
	/* Set sample frequency */
	// samplefreq = 8KHz / (1 + SMPLRT_DIV)
	// Note: DLPF_CFG on register CONFIG = 0
	dev.reg = SMPLRT_DIV;
	dev.reg_val = freq;
	nRF_i2c_wReg(&dev);
	nRF_i2c_rReg(&dev, 1);
	if (dev.reg_val != freq)
	{
		mpu9150_reset(dev);
		return false;
	}
	
	/* Enable interrupt generation by interrupt sources */
	// FIFO_OFLOW = 0
	// I2C_MST_INT_EN = 0
	// DATA_RDY_EN = 1			Enable Data Ready interrupt (all sensors data are ready)
	dev.reg = INT_ENABLE;
	dev.reg_val = INT_ENABLE_msk_data_rdy;
	nRF_i2c_wReg(&dev);
	nRF_i2c_rReg(&dev, 1);
	if (dev.reg_val != INT_ENABLE_msk_data_rdy)
	{
		mpu9150_reset(dev);
		return false;
	}
	
	
	
	
	
	dev.reg = INT_PIN_CFG;
	dev.reg_val = 0;
	dev.reg_val = INT_PIN_CFG_msk_int_rd_clear;
	//dev.reg_val |= INT_PIN_CFG_msk_int_level;			// active on low state
	dev.reg_val &= ~INT_PIN_CFG_msk_int_level;			// active on high state
	//dev.reg_val &= ~(INT_PIN_CFG_msk_latch_int_en);		// 50us
	dev.reg_val |= INT_PIN_CFG_msk_latch_int_en;		// until read
	nRF_i2c_wReg(&dev);
	
	//nRF_i2c_rReg(&dev, 1);
	//if (dev.reg_val != INT_PIN_CFG_msk_int_rd_clear)
		//error();
	
	
	
	
	/* Set gyro range */
	dev.reg = GYRO_CONFIG;
	dev.reg_val = range_gyr;
	nRF_i2c_wReg(&dev);
	nRF_i2c_rReg(&dev, 1);
	if (dev.reg_val != range_gyr)
	{
		mpu9150_reset(dev);
		return false;
	}
	
	/* Set acc range */
	dev.reg = ACCEL_CONFIG;
	dev.reg_val = range_acc;
	nRF_i2c_wReg(&dev);
	nRF_i2c_rReg(&dev, 1);
	if (dev.reg_val != range_acc)
	{
		mpu9150_reset(dev);
		return false;
	}
	
	return true;
}

void mpu9150_mag_trig_single_meas(i2c_dev_t dev)
{
	/* Temp variables */
	//uint8_t mpu9150_add;			// Current MPU9150 I2C address
	//uint8_t reg_INT_PIN_CGF;	// Current INT_PIN_CFG register's value
	
	/* Save MPU9150 address*/
	//uint8_t mpu9150_add = dev.add;
	
	/* Read and save INT_PIN_CFG */
	/*
	dev.reg = INT_PIN_CFG;	
	nRF_i2c_rReg(&dev, 1);
	reg_INT_PIN_CGF = dev.reg_val;	
	*/
	
	/* Enable I2C Pass through mode */
	/*
	dev.reg = INT_PIN_CFG;
	dev.reg_val |= INT_PIN_CFG_msk_i2c_bypass_en;
	nRF_i2c_wReg(&dev);
	*/
	
	/* AK8975 address*/
	// 0x0C << 1 = 0x18
	dev.add = 0x18;
	
	/* Trigger a single measurement */
	dev.reg = CNTL;
	dev.reg_val = CNTL_msk_single_meas;
	nRF_i2c_wReg(&dev);
	
	/* Disable I2C Pass through mode */
	/*
	dev.reg = INT_PIN_CFG;
	dev.reg_val = reg_INT_PIN_CGF;
	nRF_i2c_wReg(&dev);
	*/
	
	/* Recover MPU9150 address*/
	//dev.add = mpu9150_add;
}

void mpu9150_mag_read_meas(i2c_dev_t dev, uint8_t * axis)
{
	/* Temp variables */
	//uint8_t mpu9150_add;			// Current MPU9150 I2C address
	//uint8_t reg_INT_PIN_CGF;	// Current INT_PIN_CFG register's value
	//uint8_t i;
	
	/* Save MPU9150 address*/
	//uint8_t mpu9150_add = dev.add;
	
	/* Read and save INT_PIN_CFG */
	/*
	dev.reg = INT_PIN_CFG;	
	nRF_i2c_rReg(&dev, 1);
	reg_INT_PIN_CGF = dev.reg_val;
	*/
	
	/* Enable I2C Pass through mode */
	/*
	dev.reg = INT_PIN_CFG;
	dev.reg_val |= INT_PIN_CFG_msk_i2c_bypass_en;
	nRF_i2c_wReg(&dev);
	*/
	
	/* AK8975 address*/
	// 0x0C << 1 = 0x18
	dev.add = 0x18;
	
	/* Read the status */
	/*
	dev.reg = ST2;
	nRF_i2c_rReg(&dev, 1);
		
	if (dev.reg_val & 0xC0)
	{
		// Send zeros if an error occur
		for (i = 0; i < 6; i++)
			*(axis+i) = 0x00;
	}
	else
	*/
	{
		/* Read measurements */
		dev.reg = HXL;
		nRF_i2c_rReg(&dev, 6);
		
		/* Export and convert to big-endien */
		*(axis+0) = dev.buff[1];
		*(axis+1) = dev.buff[0];
		*(axis+2) = dev.buff[3];
		*(axis+3) = dev.buff[2];
		*(axis+4) = dev.buff[5];
		*(axis+5) = dev.buff[4];
	}
	
	/* Disable I2C Pass through mode */
	/*
	dev.reg = INT_PIN_CFG;
	dev.reg_val = reg_INT_PIN_CGF;
	nRF_i2c_wReg(&dev);
	*/
	
	/* Recover MPU9150 address*/
	//dev.add = mpu9150_add;
}