#include <nrf24le1.h>
#include <stdint.h>

#define watchdog_set_start_value(wd_low_byte, wd_high_byte)	WDSV = (wd_low_byte);\
																														WDSV = (wd_high_byte)		//Set the start value of the watchdog
																														
#define watchdog_get_start_value(wd_low_byte_p, wd_high_byte_p)		*(wd_low_byte_p) = WDSV;\
																																	*(wd_high_byte_p) = WDSV	//Get the start value of the watchdog
																																	

void nRF_set_watchdog_ms(unsigned long milliseconds)
{
	//Approximate max timeout in milliseconds is 511,992, so anything higher gets clipped here
	if(milliseconds >= 511992)
	{
		watchdog_set_start_value(0, 0); //watchdog value of 0 is the highest delay
	}
	else
	{
		//Calculate and set the start value of the watchdog
		unsigned int wd_value = (unsigned int)(((unsigned long)(milliseconds * ((unsigned long)128))) / ((unsigned long)1000));
		
		if (!wd_value)
			wd_value++;		// = 1
		
		watchdog_set_start_value((unsigned char)(wd_value & 0xFF), (unsigned char)((wd_value >> 8) & 0xFF));
	}
}