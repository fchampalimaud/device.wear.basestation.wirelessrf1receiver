#include "nRF_delays.h"

#include <stdint.h>
#include <stdbool.h>
#include <reg24le1.h>
#include <intrins.h>

/************************************************************************/
/* Delays                                                               */
/************************************************************************/
void aux_wait_10us(void) {
	uint8_t cnt = 10;
	while(cnt--)
		_nop_();
}

void aux_wait_100us(void) {
	uint8_t cnt = 113;
	while(cnt--)
		_nop_();
}

void aux_wait_1ms(void) {
	uint16_t cnt = 885;
	while(cnt--)
		_nop_();
}

void aux_wait_ms(uint16_t ms) {
	while(ms--)
		aux_wait_1ms();
}