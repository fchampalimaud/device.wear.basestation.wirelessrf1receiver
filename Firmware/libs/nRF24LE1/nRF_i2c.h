#ifndef _NRF_I2C_H_
#define _NRF_I2C_H_

#include <stdint.h>

/************************************************************************/
/* Device typedef                                                       */
/************************************************************************/
#define I2C_NBUF 16

typedef struct {
	unsigned char add;
	unsigned char reg;
	unsigned char reg_val;
	unsigned char buff[I2C_NBUF];
} i2c_dev_t;

/************************************************************************/
/* Pins definition                                                      */
/************************************************************************/
#define SCLpin 3
#define SDApin 1

#define clear_SCL		P03 = 0
#define clear_SDA		P01 = 0
#define set_SCL			P03 = 1
#define set_SDA			P01 = 1
#define toogle_SCL	P03 = !P03

#define SDA2out	P0DIR &= ~(1<<SDApin)
#define SDA2in	P0DIR |= (1<<SDApin)

/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
void nRF_i2c_init(void);
void nRF_i2c_wReg(i2c_dev_t* dev);
void nRF_i2c_wReg_nbytes(i2c_dev_t* dev, uint8_t nbytes);
void nRF_i2c_rReg(i2c_dev_t* dev, uint8_t nbytes);

#endif /* _NRF_I2C_H_ */