#ifndef _NRF_DELAYS_H__
#define _NRF_DELAYS_H__

#include <stdint.h>	

void aux_wait_10us(void);
void aux_wait_100us(void);
void aux_wait_1ms(void);
void aux_wait_ms(uint16_t ms);

#endif /* _NRF_DELAYS_H__ */