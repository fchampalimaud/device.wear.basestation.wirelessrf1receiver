#ifndef _NRF_WATCHDOG_H__
#define _NRF_WATCHDOG_H__

#include <stdint.h>

/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
void nRF_set_watchdog_ms(unsigned long milliseconds);

#endif /* _NRF_WATCHDOG_H__ */