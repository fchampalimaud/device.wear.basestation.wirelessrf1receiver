#ifndef _NRF_RADIO_H__
#define _NRF_RADIO_H__

#include <nrf24le1.h>
#include <stdint.h>
#include <stdbool.h>

/************************************************************************/
/* User                                                                 */
/************************************************************************/
#define PIPE_NUM 0
#define RX_BYTES 32

/************************************************************************/
/* nRF24L01 Instruction Definitions                                     */
/************************************************************************/
#define W_REGISTER         0x20U  /**< Register write command */
#define R_RX_PAYLOAD       0x61U  /**< Read RX payload command */
#define W_TX_PAYLOAD       0xA0U  /**< Write TX payload command */
#define FLUSH_TX           0xE1U  /**< Flush TX register command */
#define FLUSH_RX           0xE2U  /**< Flush RX register command */
#define REUSE_TX_PL        0xE3U  /**< Reuse TX payload command */
#define ACTIVATE           0x50U  /**< Activate features */
#define R_RX_PL_WID        0x60U  /**< Read RX payload command */
#define W_ACK_PAYLOAD      0xA8U  /**< Write ACK payload command */
#define W_TX_PAYLOAD_NOACK 0xB0U  /**< Write ACK payload command */
#define NOP                0xFFU  /**< No Operation command, used for reading status register */

/************************************************************************/
/* nRF24L01 Register Definitions                                        */
/************************************************************************/
#define CONFIG        0x00U  /**< nRF24L01 config register */
#define EN_AA         0x01U  /**< nRF24L01 enable Auto-Acknowledge register */
#define EN_RXADDR     0x02U  /**< nRF24L01 enable RX addresses register */
#define SETUP_AW      0x03U  /**< nRF24L01 setup of address width register */
#define SETUP_RETR    0x04U  /**< nRF24L01 setup of automatic retransmission register */
#define RF_CH         0x05U  /**< nRF24L01 RF channel register */
#define RF_SETUP      0x06U  /**< nRF24L01 RF setup register */
#define STATUS        0x07U  /**< nRF24L01 status register */
#define OBSERVE_TX    0x08U  /**< nRF24L01 transmit observe register */
#define CD            0x09U  /**< nRF24L01 carrier detect register */
#define RX_ADDR_P0    0x0AU  /**< nRF24L01 receive address data pipe0 */
#define RX_ADDR_P1    0x0BU  /**< nRF24L01 receive address data pipe1 */
#define RX_ADDR_P2    0x0CU  /**< nRF24L01 receive address data pipe2 */
#define RX_ADDR_P3    0x0DU  /**< nRF24L01 receive address data pipe3 */
#define RX_ADDR_P4    0x0EU  /**< nRF24L01 receive address data pipe4 */
#define RX_ADDR_P5    0x0FU  /**< nRF24L01 receive address data pipe5 */
#define TX_ADDR       0x10U  /**< nRF24L01 transmit address */
#define RX_PW_P0      0x11U  /**< nRF24L01 \# of bytes in rx payload for pipe0 */
#define RX_PW_P1      0x12U  /**< nRF24L01 \# of bytes in rx payload for pipe1 */
#define RX_PW_P2      0x13U  /**< nRF24L01 \# of bytes in rx payload for pipe2 */
#define RX_PW_P3      0x14U  /**< nRF24L01 \# of bytes in rx payload for pipe3 */
#define RX_PW_P4      0x15U  /**< nRF24L01 \# of bytes in rx payload for pipe4 */
#define RX_PW_P5      0x16U  /**< nRF24L01 \# of bytes in rx payload for pipe5 */
#define FIFO_STATUS   0x17U  /**< nRF24L01 FIFO status register */
#define DYNPD         0x1CU  /**< nRF24L01 Dynamic payload setup */
#define FEATURE       0x1DU  /**< nRF24L01 Exclusive feature setup */

/************************************************************************/
/* nRF24L01 Some Bit Definitions                                        */
/************************************************************************/
#define PWR_UP				0x02U
#define PRIM_RX				0x01U
#define EN_DPL				0x04U
#define EN_ACK_PAY		0x02U
#define EN_DYN_ACK		0x01U
#define RX_DR					0x40U
#define TX_DS					0x20U
#define MAX_RT				0x10U
#define RX_EMPTY			0x01U

/************************************************************************/
/* Macros for nRF24LE1                                                  */
/************************************************************************/
#define CSN_LOW() do {RFCSN = 0U; } while(false)	// radio's CSN line LOW.
#define CSN_HIGH() do {RFCSN = 1U; } while(false)	// radio's CSN line HIGH
#define CE_LOW() do {RFCE = 0U;} while(false)			// radio's CE line LOW
#define CE_HIGH() do {RFCE = 1U;} while(false)		// radio's CE line HIGH
#define HAL_NRF_HW_SPI_WRITE(d) do{SPIRDAT = (d);} while(false)
#define HAL_NRF_HW_SPI_READ() SPIRDAT							// reading the radio SPI data register
#define HAL_NRF_HW_SPI_BUSY (!(SPIRSTAT & 0x02U))	// radio SPI busy flag
/**
 * Pulses the CE to nRF24L01p for at least 10 us
 */
#define CE_PULSE() do { \
  uint8_t count; \
  count = 20U; \
  CE_HIGH();  \
    while(count--) {} \
  CE_LOW();  \
  } while(false)

/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
#define RADIO_RX_MODE 0
#define RADIO_TX_MODE 1
#define RADIO_RFPWR_m18dBm	0x00
#define RADIO_RFPWR_m12dBm	0x02
#define RADIO_RFPWR_m6dBm		0x04
#define RADIO_RFPWR_0dBm		0x06
#define RADIO_BITRATE_250Kbps	0x20
#define RADIO_BITRATE_1Mbps		0x00
#define RADIO_BITRATE_2Mbps		0x08
void nRF_radio_init(
	uint8_t mode,
	uint8_t rfpower,
	uint8_t bitrate,
	uint8_t rfch);

void nRF_radio_to_TX(void);
void nRF_radio_to_RX(void);

void nRF_radio_set_channel(uint8_t rfch);
void nRF_radio_set_data_rate(uint8_t bitrate);
void nRF_radio_set_retrans_delay(uint8_t nRetransDelay);
void nRF_radio_set_retrans(uint8_t nRetrans);

void nRF_radio_remove_auto_ack(void);

#define RADIO_POWER_DOWN 0
#define RADIO_POWER_UP 1
void nRF_radio_power_mode(uint8_t power);

uint8_t nRF_radio_read_RPD(void);
uint8_t nRF_radio_read_ARC_CNT(void);
	
void nRF_radio_clear_RX(void);
void nRF_radio_clear_TX(void);
void nRF_radio_clear_MAX_RT(void);
bool nRF_radio_check_RX(void);
bool nRF_radio_check_TX(void);
bool nRF_radio_check_MAX_RT(void);

void nRF_radio_xmit(const uint8_t *tx_pload, uint8_t length);
void nRF_radio_ack(const uint8_t *tx_pload, uint8_t length);
uint8_t nRF_radio_rcv(uint8_t *rx_pload);

/************************************************************************/
/* Radio interrupts                                                     */
/************************************************************************/
void nRF_radio_int_RX_DataReady(void);
void nRF_radio_int_TX_DataSent(void);
void nRF_radio_int_TX_MaxRetrans(void);

#endif /* _NRF_RADIO_H_ */