/*
 1: Projects -> Options for file '...'... -> C51 -> Include Paths
 2: Include Paths equal to: C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\hal;C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\hal\nrf24le1;C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\hal\nrf24l01p;C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\compiler\common;C:\Nordic Semiconductor\nRFgo SDK 2.3.0.10040\source_code\compiler\c51
*/
#include <stdint.h>		// uint8_t, int8_t, uint16_t, int16_t, uint32_t and int32_t
#include <stdbool.h>	// bool, true and false
#include <reg24le1.h>	// Register, Pins and interrupt vectors
#include <intrins.h>	// _nop_ ();

/************************************************************************/
/* External sources                                                     */
/************************************************************************/
#include "..\libs\protocol\protocol.h"
#include "..\libs\nRF24LE1\nRF_uart_with_flowcontrol.h"
#include "..\libs\nRF24LE1\nRF_radio.h"
#include "..\libs\nRF24LE1\nRF_delays.h"
#include "..\libs\nRF24LE1\nRF_watchdog.h"


/************************************************************************/
/* Firmware version                                                     */
/************************************************************************/
#define FW_VERSION_MAJOR 4
#define FW_VERSION_MINOR 0

/************************************************************************/
/* Hardware version                                                     */
/************************************************************************/
#define HW_VERSION_MAJOR 1
#define HW_VERSION_MINOR 0

/************************************************************************/
/* Frequency center                                                     */
/************************************************************************/
#define CH_FREQ_START 43	// 2.443 GHz
// Note: 250Kbps reduces sensitivity on frequencies multiples of 16 (2400, 2416 and so on)

/************************************************************************/
/* Globals                                                              */
/************************************************************************/
wake_up_t wake_up;
stim1_t stim;

uint8_t packet[36];

uint8_t xdata wakeup_channel;
uint8_t xdata wakeup_mbits;
uint16_t xdata led_tgl_counter;
uint16_t xdata led_tgl_counter_target;

bool xdata use_LED;

bool stim_to_send = false;
uint16_t wake_up_counter;

/************************************************************************/
/* Pins definition                                                      */
/************************************************************************/
#define EN_pin					0			// !EN (in)
#define EN_portpin			P00		// !EN (in)
#define SAMPLE_pin			1			// !SAMPLE (out)
#define SAMPLE_portpin	P01		// !SAMPLE (out)
#define LED_pin					3			// 
#define LED_portpin			P03

#define set_SAMPLE	SAMPLE_portpin = 1
#define clr_SAMPLE	SAMPLE_portpin = 0
#define tgl_SAMPLE	SAMPLE_portpin = !SAMPLE_portpin
#define set_LED			LED_portpin = 0
#define clr_LED			LED_portpin = 1
#define tgl_LED			LED_portpin = !LED_portpin

#define read_EN (EN_portpin ? true : false)

void pins_init(void)
{
	P0DIR &= ~(1<<SAMPLE_pin);		// To out (default: normal drive strength)
	P0DIR &= ~(1<<LED_pin);				// To out (default: normal drive strength)
	
	P0DIR |= (1<<EN_pin);					// To in
	P0CON = 0x50 | EN_pin;				// pull-up resitor connected
	//P0CON = 0x10 | EN_pin;				// No pull up/down resistors
	
	set_SAMPLE;
	clr_LED;
}

void wakeup_irq() interrupt INTERRUPT_WUOPIRQ
{
	/*
	PWRDWN = 0;
	
	if (!read_EN)
	{
		while(1) {aux_wait_ms(200); tgl_LED;}
		set_LED;
		nRF_set_watchdog_ms(1);
	}
	*/
}

void enable_high() interrupt INTERRUPT_IPF
{
	//tgl_LED;
}

/************************************************************************/
/* CLK                                                                  */
/************************************************************************/
typedef enum
{
  CLK_XOSC16M_AND_RCOSC16M = 0,
  CLK_RCOSC16M = 1,
  CLK_XOSC16M = 2
} clk_source_t;

clk_source_t clk_get_16M_source(void)
{
  clk_source_t clock_source;
	
  if(CLKLFCTRL & (uint8_t)0x08U)
    clock_source = CLK_XOSC16M;
  else
    clock_source = CLK_RCOSC16M;

  return clock_source;
}

/************************************************************************/
/* LED                                                                  */
/************************************************************************/
bool led_timer_operating_state = false;

void led_timer(bool operating)
{
	if (led_timer_operating_state == operating)
		return;
	
	led_timer_operating_state = operating;
	
	/* Configure clock for RTC2 and watchdog (CLKLF) */
	CLKLFCTRL = 0x01;														// select RCOSC32K
	while(!CLKLFCTRL & 0x40);										// wait until CLKLF is ready to use
	while(CLKLFCTRL & 0x80);										// make sure CLKLF goes to 1
	while(!CLKLFCTRL & 0x80);										// make sure CLKLF return to 0
	
	/* Configure TICK interrupt (it's the interrupt from RTC2) */
	/* (RTC2CMP + 1) / 32768 = time [s] */
	if (operating)
	{
		RTC2CMP1 = 0x0C;													// compare value MSByte (0.1 seconds)
		RTC2CMP0 = 0xCC;													// compare value LSByte
	}
	else
	{		
		RTC2CMP1 = 0x7F;													// compare value MSByte (1 second)
		RTC2CMP0 = 0xFF;													// compare value LSByte
	}
	
	RTC2CON = 0x07;															// compare mode 11 (timer counter resets)
																							// enable CLKLF to RTC2 core
	IEN1 |= 0x20;																// enable TICK interrupt
	
	//IP0 = 0x20;																// configure INTERRUPT_TICK to Level 1 */
}

uint8_t rx_timeout;
void wakeup_tick() interrupt INTERRUPT_TICK
{
	//tgl_LED;
	if (++rx_timeout >= 2)
	{
		if (use_LED)
			set_LED;
		rx_timeout = 0;
		led_tgl_counter = 0;
	}
		
}

/************************************************************************/
/* main()                                                               */
/************************************************************************/
uint8_t payload[40];

void main()
{
	/* Configure clock for MCU (Cclk) */
	CLKCTRL = CLK_XOSC16M << 4;									// XOSC16M not keeped on in Register Retention mode
																							// start only the XOSC16M
																							// frequency equal to 16 MHz
	while(clk_get_16M_source() != CLK_XOSC16M);	// wait until XOSC16M is active/running

	/* Initialize pins */
	OPMCON = 0;					// Open latch	
	pins_init();
	
	/* Initialize radio */
	nRF_radio_init(
		RADIO_RX_MODE,
		RADIO_RFPWR_0dBm,
		wakeup_mbits,
		wakeup_channel);	
	
	/* Initializes the UART with 500Kbits */
	nRF_uart_init(1023);
	
	/* Initialize LED timer */
	led_timer(true);
	led_timer(false);	// 1 second
	
	/* Initialize packet header */
	packet[0] = 'w';
	packet[1] = '>';	
	packet[2] = ':';
	
	/* Reset PWRDWN */
	PWRDWN = 0;					// clear PWRDWN			
	
	/* Check if device is enabled */
	if(read_EN)
	{
		/* Device is enabled */
		if (use_LED)
			set_LED;				// Set LED
	}
	else
	{
		/* Device is disabled */
		clr_LED;
		P0DIR |= (1<<RTS_pin);
											// CTS to input
		use_LED = true;
		WUOPC0 = 0x01;		// Wake up on pin P2.0 active-high 
		CLKCTRL = CLK_XOSC16M_AND_RCOSC16M << 4;
											// Wake up RCOSC16M
		OPMCON |= 0x02;		// Lock latch
		while(1)
			PWRDWN = 0x01;	// Deep Sleep
	}
		
	/* Enable global interrupts */
  EA = 1;
	
	/* Infinite loop */
	while(1)
	{
		/* Check if device is still selected */
		if (!read_EN)
		{
			/* Device must shutdown */
			clr_LED;
			P0DIR |= (1<<RTS_pin);
												// CTS to input
			use_LED = true;
			WUOPC0 = 0x01;		// Wake up on pin P0.0 active-high 
			CLKCTRL = CLK_XOSC16M_AND_RCOSC16M << 4;
												// Wake up RCOSC16M
			OPMCON |= 0x02;		// Lock latch
			while(1)
				PWRDWN = 0x01;	// Deep Sleep
		}
		
		nRF_uart_loop();
	}	
}


/************************************************************************/
/* uart RX routine                                                      */
/************************************************************************/
uint8_t pointer = 0;
uint8_t uart_rx[CMD_FIXED_LEN];

uint8_t checksum = 0;

uint16_t samples_to_ignore;

uint8_t tx_wait;

void nRF_uart_rcv(uint8_t byte)
{
	uint16_t id;
	
	if (!pointer)
	{
		if (!(byte & ~0x63))
		{
			*(uart_rx) = byte;
			pointer++;
			
			RF = 0;	// Disable all RF interrupts
		}
	}
	else
	{
		*(uart_rx + pointer) = byte;
		
		if (++pointer == CMD_FIXED_LEN)
		{
			pointer = 0;
			
			switch(*(uart_rx))
			{
				/* wake up */
				case 0x60:
					wake_up = *((wake_up_t*)(uart_rx));
					
					/* Config to wait 6 seconds before start sending to PC */	
					/*
					switch(wake_up.freq)
					{
						case 0: // 50 Hz
							samples_to_ignore = 300;
							break;						
						case 1: // 100 Hz
							samples_to_ignore = 600;
							break;						
						case 2: // 200 Hz
							samples_to_ignore = 1200;
							break;
					}
					*/
					samples_to_ignore = 1;
				
					clr_LED;
					
					stim_to_send = false;
					
					id = wake_up.id_LSByte;
					id |= (wake_up.id_chmask & 0xF8) << 5;
					
					CE_HIGH();									// RF from RX Mode to Standby-I
					//aux_wait_100us;
					//aux_wait_100us;
					nRF_radio_set_data_rate(RADIO_BITRATE_250Kbps);
					nRF_radio_set_retrans(0);		// Change RF settings
					nRF_radio_set_channel(CH_FREQ_START + (id%10) * 2);
					wakeup_channel = CH_FREQ_START + (id%10) * 2;
					//aux_wait_100us;
					//aux_wait_100us;
					nRF_radio_to_TX();
					//aux_wait_100us;
					//aux_wait_100us;				
					CE_LOW();
					//aux_wait_100us;
					//aux_wait_100us;	
					
					nRF_radio_clear_TX();
					nRF_radio_clear_RX();
					nRF_radio_clear_MAX_RT();
					
					use_LED = (wake_up.freq & 0x80) ? true : false;
					wake_up.freq &= 0x7F;
					
					switch ((wake_up.freq & 0x60) >> 5)
					{
						case 0:
							wakeup_mbits = RADIO_BITRATE_2Mbps;
							break;						
						case 1:
							wakeup_mbits = RADIO_BITRATE_1Mbps;
							break;						
						case 2:
							wakeup_mbits = RADIO_BITRATE_250Kbps;
							break;
					}
					
					/* Calculate led counter and target */
					switch (wake_up.freq & 0x0F)
					{
						case 0:
							led_tgl_counter_target = 50 / 2;
							break;
						case 1:
							led_tgl_counter_target = 100 / 2;
							break;
						case 2:
							led_tgl_counter_target = 200 / 2;
							break;
					}
					led_tgl_counter = 0;
					
					/* Packet are send as fast as possible (new packet is sent when MAX_RT is set, meaning that no ACK as received) */
					/* If the MAX_RT is not received, it means that the Wear sensor reply with an ACK, i.e., it's waken */
					/* 2 Mbps   - 490 us interval --> For around 3.56 seconds of streaming use "wake_up_counter < 7000" */
					/* 1 Mbps   - 550 us interval --> For around 3.56 seconds of streaming use "wake_up_counter < 6236" */
					/* 250 Kbps - 900 us interval --> For around 3.56 seconds of streaming use "wake_up_counter < 3811" */
					for (wake_up_counter = 0; wake_up_counter < 3811 /*~3.5sec*/; wake_up_counter++)
					{
						nRF_radio_xmit((uint8_t*)(&wake_up), CMD_FIXED_LEN);
						CE_PULSE();
						
						tx_wait = 0;
						
						/* tx_wait < 250 --> 2.2 ms */
						/* tx_wait < 150 --> 1.3 ms */
						/* tx_wait < 100 --> 800 us */
						/* This code only work with 2Mbps and 1Mbps */
						while(!nRF_radio_check_MAX_RT() && tx_wait < 150)
						{							
							tx_wait++;
							if (tx_wait == 150)
								wake_up_counter = 0xFFFF - 1;
						}
						
						nRF_radio_clear_TX();
						nRF_radio_clear_MAX_RT();
						
						if ((wake_up_counter & 0x2FF) == 0 || (wake_up_counter & 0x2FF) == 300 || (wake_up_counter & 0x2FF) == 500)
						{
							if (use_LED)
								set_LED;
						}
						if ((wake_up_counter & 0x2FF) == 200 || (wake_up_counter & 0x2FF) == 400)
						{
							if (use_LED)
								clr_LED;
						}
					}
					
					nRF_radio_to_RX();
					
					nRF_set_watchdog_ms(1);
					while(1);
			
				/* disable rx */
				case 0x61:
					CE_LOW();
					led_timer(false);
					if (use_LED)
						set_LED;
					break;
			
				
				/* stim */
				case CMD_STIM_1:
				case CMD_STIM_2:
				case CMD_STIM_3:
					checksum = 0;
					for (;pointer < 7; pointer ++)
						checksum += uart_rx[pointer];
					pointer = 0;
				
					if (uart_rx[7] == checksum)
					{					
						stim = *((wake_up_t*)(uart_rx));
						stim_to_send = true;
						
						/*
						if (stim_to_send == false)
						{
							nRF_radio_ack((uint8_t*)(&stim), CMD_FIXED_LEN);
						}
						*/
					}
					break;
			}
			
			RF = 1;	// Re-enable all RF interrupts			
		}
	}
}


/************************************************************************/
/* radio service routines                                               */
/************************************************************************/
void nRF_radio_int_RX_DataReady(void)
{
	/* TX */
	/*
	packet[0] = nRF_radio_rcv(packet+1);
	nRF_uart_xmit(packet, packet[0]+1);
	*/
	
	/* RX */	
	uint8_t len, i;
	uint8_t checksum = 0;
	
	nRF_uart_disable;

	//if (samples_to_ignore == 0)
		//clr_SAMPLE;
	//rx_timeout = 0;
		//clr_LED;
	//else
		//led_timer(true);
	//if (samples_to_ignore == 1)
		//led_timer(true);
	
	len = nRF_radio_rcv(packet+3);
	if (len == 0)
		return;
	
	clr_SAMPLE;
	rx_timeout = 0;
	
	if (packet[3] == 0x02)
		packet[6] = nRF_radio_read_RPD();
	else if (packet[3] == 0x12)
		packet[6] = FW_VERSION_MAJOR;
	else if (packet[3] == 0x13)
		packet[6] = FW_VERSION_MINOR;
	else if (packet[3] == 0x14)
		packet[6] = HW_VERSION_MAJOR;
	else if (packet[3] == 0x15)
		packet[6] = HW_VERSION_MINOR;
	
		
	//if (samples_to_ignore == 0)
		nRF_uart_xmit(packet, len+3);
	
	for (i = 0; i < len; i++)
		checksum += packet[i+3];
	
	//if (samples_to_ignore == 0)
		nRF_uart_xmit(&checksum, 1);
	
	if(stim_to_send)
	{
		nRF_radio_ack((uint8_t*)(&stim), CMD_FIXED_LEN);
		stim_to_send = false;
	}
	/*
	stim_to_send = false;
	*/
	
	//aux_wait_100us();
	//aux_wait_100us();
	
	//if (samples_to_ignore == 0)
	
	/* Toggle LED */
	if (++led_tgl_counter == led_tgl_counter_target)
	{
		led_tgl_counter = 0;
		if (use_LED)
			tgl_LED;
	}
	
	
	set_SAMPLE;
	//set_LED;
	
	//if (samples_to_ignore > 0)
		//samples_to_ignore--;
	
	nRF_uart_enable;
}

void nRF_radio_int_TX_DataSent(void)
{
	uint8_t stimulation_acknowledge[5] = {'a', 'a', 'a', 'a', 'a'};
	
	nRF_uart_disable;
	nRF_uart_xmit(stimulation_acknowledge, 5);
	nRF_uart_enable;
}

void nRF_radio_int_TX_MaxRetrans(void)
{}



void blink_LED(uint8_t on, uint8_t off)
{
	uint8_t i = 0;
	
	while(1)
	{		
		set_LED;
		for (i = 0; i < on; i++)
			aux_wait_1ms();
		clr_LED;
		for (i = 0; i < off; i++)
			aux_wait_1ms();
	}
}

//EXT_INT0_ISR() {}
T0_ISR() {blink_LED(60, 240);}
AES_RDY_ISR() {blink_LED(80, 240);}
T1_ISR() {blink_LED(100, 240);}
//UART0_ISR() {}
T2_ISR() {blink_LED(120, 240);}
RF_RDY_ISR() {blink_LED(140, 240);}
//NRF_ISR() {}
SER_ISR() {blink_LED(160, 240);}
//WUOP_ISR() {}
//MISC_ISR() {}
ADC_ISR() {blink_LED(180, 240);}
//TICK_ISR() {}
